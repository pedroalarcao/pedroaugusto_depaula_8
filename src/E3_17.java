import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

class EllipseComponent extends JComponent{

    public void paintComponent(Graphics g){

        Graphics2D g2 = (Graphics2D) g;

        Ellipse2D.Double ellipse = new Ellipse2D.Double(0, 0, 300, 300);


        int width = getWidth();
        int height = getHeight();

        g2.setColor(new Color(20,150,50));
        g2.fillOval(0, 0,width, height);
        g2.setColor(Color.BLACK);
        g2.drawOval(0,0, width,height);

    }

}

public class E3_17{


    public static void main(String[] args) {

        JFrame frame = new JFrame();

        frame.setSize(600, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);


        EllipseComponent ellipse = new EllipseComponent();

        frame.add(ellipse);




    }
}

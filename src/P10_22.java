import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

class Car{

    private int xLeft;
    private int yTop;


    public void draw(Graphics2D g2){

        Rectangle body = new Rectangle(xLeft, yTop + 10, 60, 10);

        Ellipse2D.Double frontTire = new Ellipse2D.Double(xLeft + 10, yTop + 20, 10, 10);
        Ellipse2D.Double rearTire = new Ellipse2D.Double(xLeft + 40, yTop + 20, 10, 10);

        Point2D.Double r1 = new Point2D.Double(xLeft + 10, yTop + 10);
        Point2D.Double r2 = new Point2D.Double(xLeft + 20, yTop);
        Point2D.Double r3 = new Point2D.Double(xLeft + 40, yTop);
        Point2D.Double r4 = new Point2D.Double(xLeft + 50, yTop + 10);

        Line2D.Double frontWindshield = new Line2D.Double(r1, r2);
        Line2D.Double roofTop = new Line2D.Double(r2, r3);
        Line2D.Double rearWindshield = new Line2D.Double(r3, r4);

        g2.draw(body);
        g2.draw(frontTire);
        g2.draw(rearTire);
        g2.draw(frontWindshield);
        g2.draw(roofTop);
        g2.draw(rearWindshield);

    }

    public void translate(int dx, int dy){

        xLeft += dx;
        yTop += dy;

    }
}

class CarComponent extends JComponent {

    Car car1 = new Car();


    public void paintComponent(Graphics g){

        Graphics2D g2 = (Graphics2D) g;


        int x = getWidth() - 60;
        int y = getHeight() - 30;

        car1.draw(g2);
    }

    public void moveCarBy(int dx, int dy){
        car1.translate(dx, dy);
        repaint();
    }

}

class CarFrame extends JFrame{


    private CarComponent scene;

    class TimerListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            scene.moveCarBy(1,1);
        }
    }

    public CarFrame(){

        scene = new CarComponent();
        add(scene);

        setSize(400,400);

        ActionListener listener = new TimerListener();

        final int DELAY = 10;
        Timer t = new Timer(DELAY, listener);
        t.start();
    }
}
public class P10_22 {
    public static void main(String[] args) {

        JFrame frame = new CarFrame();

        frame.setTitle("VROOMVROOM");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);



    }
}

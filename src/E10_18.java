import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class ClickListener implements ActionListener{

    int countA = 0;
    int countB = 0;

    public void actionPerformed(ActionEvent event){

        if (event.getActionCommand() == "button A was clicked"){
            countA++;
            System.out.println("I was clicked " + countA + " times!");
            JOptionPane.showMessageDialog(null, "I was clicked " + countA + " times!");
        }
        else if(event.getActionCommand() == "button B was clicked"){
            countB++;
            System.out.println("I was clicked " + countB + " times!");
            JOptionPane.showMessageDialog(null, "I was clicked " + countB + " times!");
        }
    }

}

public class E10_18 {

    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 300;

    public static void main(String[] args) {

        JFrame frame = new JFrame();
        JPanel panel = new JPanel();

        JButton buttonA = new JButton("Button A: CLICK ME");
        buttonA.setActionCommand("button A was clicked");
        JButton buttonB = new JButton("Button B: NAHH, CLICK ME");
        buttonB.setActionCommand("button B was clicked");


        panel.add(buttonA);
        panel.add(buttonB);
        frame.add(panel);


        ActionListener listener = new ClickListener();
        buttonA.addActionListener(listener);
        buttonB.addActionListener(listener);

        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
